package library;

import java.text.SimpleDateFormat;
import java.util.Scanner;

import library.borrowbook.BorrowBookUI;
import library.borrowbook.BorrowBookControl;
import library.entities.Book;
import library.entities.Calendar;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;
import library.fixbook.FixBookUI;
import library.fixbook.FixBookControl;
import library.payfine.PayFineControl;
import library.payfine.PayFineUI;
import library.payfine.PayFineControl;
import library.returnBook.ReturnBookControl;
import library.returnBook.ReturnBookUI;
import library.returnBook.ReturnBookControl;

public class Main {

    private static Scanner input;
    private static Library library;
    private static String menu;
    private static Calendar calender;
    private static SimpleDateFormat simpleDateFormat;

    private static String getMenu() {
        StringBuilder sb = new StringBuilder();

        sb.append("\nLibrary Main Menu\n\n")
                .append("  M  : add member\n")
                .append("  LM : list members\n")
                .append("\n")
                .append("  B  : add book\n")
                .append("  LB : list books\n")
                .append("  FB : fix books\n")
                .append("\n")
                .append("  L  : take out a loan\n")
                .append("  R  : return a loan\n")
                .append("  LL : list loans\n")
                .append("\n")
                .append("  P  : pay fine\n")
                .append("\n")
                .append("  T  : increment date\n")
                .append("  Q  : quit\n")
                .append("\n")
                .append("Choice : ");

        return sb.toString();
    }

    public static void main(String[] args) {
        try {
            input = new Scanner(System.in);
            library = Library.getInstance();
            calender = Calendar.getInstance();
            simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

            for (Member m : library.listMembers()) {
                setOutput(m);
            }
            setOutput(" ");
            for (Book b : library.listBooks()) {
                setOutput(b);
            }

            menu = getMenu();

            boolean e = false;

            while (!e) {

                setOutput("\n" + simpleDateFormat.format(calender.getDate()));
                String c = getInput(menu);

                switch (c.toUpperCase()) {

                    case "M":
                        addMember();
                        break;

                    case "LM":
                        listMembers();
                        break;

                    case "B":
                        addBook();
                        break;

                    case "LB":
                        listBooks();
                        break;

                    case "FB":
                        fixBooks();
                        break;

                    case "L":
                        borrowBook();
                        break;

                    case "R":
                        returnBook();
                        break;

                    case "LL":
                        listCurrentLoans();
                        break;

                    case "P":
                        payFines();
                        break;

                    case "T":
                        incrementDate();
                        break;

                    case "Q":
                        e = true;
                        break;

                    default:
                        setOutput("\nInvalid option\n");
                        break;
                }

                Library.save();
            }
        } catch (RuntimeException e) {
              setOutput(e);
        }
        setOutput("\nEnded\n");
    }

    private static void payFines() {
        new PayFineUI(new PayFineControl()).run();
    }

    private static void listCurrentLoans() {
        setOutput("");
        for (Loan loan : library.listCurrentLoans()) {
            setOutput(loan + "\n");
        }
    }

    private static void listBooks() {
        setOutput("");
        for (Book book : library.listBooks()) {
            setOutput(book + "\n");
        }
    }

    private static void listMembers() {
        setOutput("");
        for (Member member : library.listMembers()) {
            setOutput(member + "\n");
        }
    }

    private static void borrowBook() {
        new BorrowBookUI(new BorrowBookControl()).run();
    }

    private static void returnBook() {
        new ReturnBookUI(new ReturnBookControl()).run();
    }

    private static void fixBooks() {
        new FixBookUI(new FixBookControl()).run();
    }

    private static void incrementDate() {
        try {
            int days = Integer.valueOf(getInput("Enter number of days: ")).intValue();
            calender.incrementDate(days);
            library.checkCurrentLoans();
            setOutput(simpleDateFormat.format(calender.getDate()));

        } catch (NumberFormatException e) {
            setOutput("\nInvalid number of days\n");
        }
    }

    private static void addBook() {

        String  author = getInput("Enter author: ");
        String title = getInput("Enter title: ");
        String callNumber = getInput("Enter call number: ");
        Book book = library.addBook(author, title, callNumber);
        setOutput("\n" + book + "\n");

    }

    private static void addMember() {
        try {
            String lastName = getInput("Enter last name: ");
            String firstName = getInput("Enter first name: ");
            String emailAddress = getInput("Enter email address: ");
            int phoneNumber = Integer.valueOf(getInput("Enter phone number: ")).intValue();
            Member member = library.addMember(lastName, firstName, emailAddress, phoneNumber);
            setOutput("\n" + member + "\n");

        } catch (NumberFormatException e) {
            setOutput("\nInvalid phone number\n");
        }

    }

    private static String getInput(String prompt) {
        System.out.print(prompt);
        return input.nextLine();
    }

    private static void setOutput(Object object) {
        System.out.println(object);
    }

}
